<img src="images/ulb_logo.jpg" width=60 height=60 />

# Hello World Tekton Pipelines !

[Tekton](https://github.com/tektoncd/pipeline) is a Knative-based framework for CI/CD pipelines, but it's unique due to its decoupled nature—meaning that one pipeline can be used to deploy to any Kubernetes cluster across multiple hybrid cloud providers. In addition, Tekton stores everything related to a pipeline as custom resources (CRs) within the cluster, allowing pieces to be used across multiple pipelines.

## Creating our first Task
For our introduction to Tasks, let's start off with a simple "Hello World" Task. Task resources are essential building block components for creating a Pipeline, and this first Task will allow us to use a Red Hat Universal Base Image and echo a "Hello World". To begin, let's open the file `task-hello.yaml` in the `01-tasks folder`:

```yaml
apiVersion: tekton.dev/v1beta1
kind: Task
metadata:
  name: hello
spec:
  steps:
    - name: say-hello
      image: registry.access.redhat.com/ubi8/ubi
      command:
        - /bin/bash
      args: ['-c', 'echo Hello World']
```
You'll notice several details above, from the kind being a Task, to the step of "say-hello", and the args being to simply output an echo command to the console. Let's apply this Task to our cluster, similar to any other Kubernetes object:

```shell
oc apply -f ./01-tasks/task-hello.yaml
```

```shell
tkn task start --showlog hello
```

Great work! After running this `tkn` command, you'll soon see an output from the Task in the console like such:
```
TaskRun started: hello-run-6cgf5
Waiting for logs to be available...
[say-hello] Hello World
```

## Creating our first Pipeline
Now that you understand the concept of tasks, let's dive into creating a Pipeline. For consistency, Tasks are meant for single actions, while a Pipeline is a series of Tasks that can be run either in parallel or sequentially.

```yaml
apiVersion: tekton.dev/v1alpha1
kind: Pipeline
metadata:
  name: hello-pipeline
spec:
  tasks:
    - name: pipeline-hello-task
      taskRef:
        name: hello
```
We're now ready to apply the new Pipeline to our cluster, and officially start the Pipeline. Using `tkn pipeline start`, we create a PipelineRun resource automatically with a random name:

```shell
oc apply -f ./demo/04-tasks.yaml
```

```shell
oc apply -f ./demo/05-pipeline.yaml
```
```shell
tkn pipeline start say-things --showlog
```
Congrats! You'll notice the console has to output the logs from the PipelineRun.