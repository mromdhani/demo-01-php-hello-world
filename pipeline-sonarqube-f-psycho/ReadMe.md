<img src="images/ulb_logo.jpg" width=60 height=60 /></div>

This is a pipeline including a SonarQube scanning task for a f-psycho applications : **cne** and **padi**.

- The pipelinerun `01-pipelinerun-psycho-cne.yaml` processes the `cne` application.

- The pipelinerun `01-pipelinerun-psycho-padi.yaml` processes the `padi` application.

The Sonarqube URL is : <https://sonarqube-9-community-edition-f-psycho.apps.dev.okd.hpda.ulb.ac.be/projects>
username: admin
password: sonar

The yaml manifests for the deployment of the SonarQube server are located in `00-config\sonarqube-9-community-deploy.yaml`.  