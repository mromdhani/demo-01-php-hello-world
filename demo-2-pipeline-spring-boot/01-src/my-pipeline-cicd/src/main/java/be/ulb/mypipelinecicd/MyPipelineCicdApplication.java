package be.ulb.mypipelinecicd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyPipelineCicdApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyPipelineCicdApplication.class, args);
    }

}
