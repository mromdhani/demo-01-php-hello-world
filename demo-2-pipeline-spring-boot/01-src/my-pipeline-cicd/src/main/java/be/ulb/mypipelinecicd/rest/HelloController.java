package be.ulb.mypipelinecicd.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {


    @RequestMapping("/")
    public String home() {
        return "Hello from Spring Micro-service ! Updated ....";
    }
}
