<img src="images/ulb_logo.jpg" width=60 height=60 /></div>

 This project contains a pipeline for an example of Java/Spring application.

 - The `O1-src` folder contains the source code of the application.
 - The `O2-cicd` folder contains the CI/CD Pipeline artifacts.